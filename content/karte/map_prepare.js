function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    //console.log('Query variable %s not found', variable);
}

function getSecondPart(str) {
    return str.split('=')[1];
}

function getSurfbarLink(str, surfbar, mapped) {
    if (surfbar == "true" && mapped == "true") {
        if (str.startsWith("Talsperre")) {
            var link_name = str.replace(' ', '_')
        } else {
            var link_name = str.split(' ')[0];
        }
        return '&#10140; <a href="/spots/' + link_name.toLowerCase() + '">Detailinfos und Liste der Spots</a>'
    } else {
        return ""
    }
}

// init map
surfspot = getQueryVariable('surfspot')
if (!(surfspot)) {
    surfspot = "main";
}
lat_start = getQueryVariable('lat');
lng_start = getQueryVariable('lng');
zoom_start = getQueryVariable('zoom');
if ((!(lat_start)) || (!(lng_start))) {
    if (surfspot == "main") {
        lng_start = 14.366437;
        lat_start = 51.420588;
        zoom_start = 9.5;
    } else {
        lng_start = 14.366437;
        lat_start = 51.350588;
        zoom_start = 10.9;
    }
}

var url = 'surfbar_WGS1984_cleaned.geojson?version=1';
