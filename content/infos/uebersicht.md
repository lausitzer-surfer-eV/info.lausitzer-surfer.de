+++
title = "Verein & Ziele"
description = "Wir haben verschiedene Informationsseiten und Netzwerke, welche von Mitgliedern und nicht-Mitgliedern genutzt werden können, um Informationen zu verteilen."
weight = 10
draft = false
toc = false
bref = "Übersicht zum Verein mit Links und Zusammenfassung zu allen Seiten."
+++

{{< rawhtml >}}

<figure>
    <img src="/images/partwitzer.jpg" 
      alt="Partwitzer See">
<figcaption>Partwitzer See, Sebastian Siwek</figcaption>
</figure>

{{< /rawhtml >}}

Aus der [Satzung des Vereins](https://lausitzer-surfer.de/pdf/01_Satzung.pdf) (§2 Abs. 1). Die Ziele sind es:


* Nachwuchsförderung durch Vernetzung seiner Mitglieder, insbesondere im Anschluß an die Surfschul-Grundkursausbildung in der Lausitz und im Umland zu betreiben und Informationen über Surfstrände, Gewässer und Regeln anzubieten und zu verbreiten,
* eine demokratische Interessenvertretung und Ansprechpartner zu schaffen, insbesondere zum Mitwirken bei Entwicklung, Ausbau, Beratung und Gestaltung von Surf-Zugängen und der Gewässernutzung im Gebiet der Lausitzer Seen und
* seine Mitglieder körperlich bei der Erhaltung von Fitness und Gesundheit zu unterstützen und die gegenseitige Achtung, Humanität und Kameradschaftlichkeit im Sport zu fördern, insbesondere durch Jugend-, Breiten- und Wettkampfsport.

------

Für die Vernetzung, Kommunikation von Informationen und Vereinsorganisation haben wir für unsere Mitglieder und nicht-Mitglieder verschiedene Seiten & Dienste.

## {{< rawhtml >}}<i class="fa fa-plane-arrival"></i>{{< /rawhtml >}}  [lausitzer-surfer.de](https://lausitzer-surfer.de/)

Hauptseite mit allen Informationen. Wenn Ihr wissen wollt, wie Ihr diese Seite bearbeiten könnt, schaut
unter [Vereinsorga](/infos/vereinsorga/).

-------

## {{< rawhtml >}}<i class="fa fa-clipboard-list"></i>{{< /rawhtml >}} [info.lausitzer-surfer.de](https://info.lausitzer-surfer.de/)

Informationen und Vereinsorga.

-------

## {{< rawhtml >}}<i class="fa fa-microphone"></i>{{< /rawhtml >}} [live.lausitzer-surfer.de](https://live.lausitzer-surfer.de/)

Live-Berichte und Bilder von den Spots!  
ACHTUNG: Noch nicht aktiv.


-------

## {{< rawhtml >}}<i class="fa fa-address-card"></i>{{< /rawhtml >}} [mitglieder.lausitzer-surfer.de](https://mitglieder.lausitzer-surfer.de/)

Mitglieder-Selbstverwaltung, Nutzermanagement und Online-Registrierung.

Hier könnt Ihr Euch online registrieren. Die [Offline-Anmeldung](https://lausitzer-surfer.de/pdf/03_Aufnahmeantrag.pdf) ist nicht mehr nötig, stellt aber eine Alternative für jene dar, die sich nicht online registrieren wollen oder können.

{{< rawhtml >}}
<details><summary><strong>Daten, welche geändert werden können</strong></summary>
<ul>
<li>E-Mail Adresse*</li>
<li>Passwort*</li>
<li>Telefon</li>
<li>Beschäftigung</li>
<li>Geburtsdatum*</li>
<li>Lastschrift</li>
<li>Anschrift:*</li>
<ul>
<li>Straße</li>
<li>PLZ</li>
<li>Ort</li>
</ul>
<li>Für Fördermitglieder:
<ul>
<li>Höhe des freiwilligen Betrags
</ul>
<li>Interesse (Auswahl jeweils : <code>Daran interessiert</code>, <code>Anfänger</code>, <code>Fortgeschritten</code>, <code>Profi</code>):
<ul>
<li>Windsurfen</li>
<li>Surffoilen</li>
<li>Wingfoilen</li>
<li>Kitesurfen</li>
<li>Stand Up Paddling (SUP)</li>
<li>SUP-Foilen</li>
<li>Surfen</li>
</ul>
<li>Annahme der Satzung und Nutzungsbedingungen*</li>
<li>Lastschrift für Mitgliedsbeitrag</li>
<ul>
<li>IBAN/ BIC</li>
</ul>
</ul>

<p>Einträge welche mit * markiert sind als Pflichtangaben.</p>
</details><br>

{{< /rawhtml >}}

Ausserdem könnt Ihr über einen Klick aus dem Verein austreten und das Mitglieskonto löschen.

-------

## {{< rawhtml >}}<i class="fa fa-wind"></i>{{< /rawhtml >}} [info.lausitzer-surfer.de/spots](https://info.lausitzer-surfer.de/spots/)

Detailinfos zu Gewässern, Spots und Regeln.  
ACHTUNG: Die Gewässer- und Spot-Beschreibungen sind derzeit noch im Aufbau und nicht vollständig.

-------

## {{< rawhtml >}}<i class="fa fa-map"></i>{{< /rawhtml >}} [info.lausitzer-surfer.de/karte](https://info.lausitzer-surfer.de/karte)

Online-Karte mit allen Gewässern und Spots. 
ACHTUNG: Die Karte ist derzeit noch im Aufbau und nicht vollständig.
