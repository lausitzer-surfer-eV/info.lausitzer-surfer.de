+++
title = "ToDo"
description = "Übersicht was gerade alles zu tun ist."
weight = 10
draft = false
toc = true
bref = "Es gibt immer Sachen zu tun. Hier findest Du eine Übersicht."
+++

### [Karte](#h-karte)

Wir wollen als erstes eine Karte und Übersicht zu den Surf-Gewässern der Lausitz erstellen. Viele Regeln sind leider nur sehr versteckt zu finden. Wo ist Kite-Surfen erlaubt? Wann darf man auf dem Senftenberger See surfen (nicht im Winter!). Hierzu müssen wir 

* zuerst die Gemeinden und Gewässerverantwortlichen kontaktieren, unseren Verein vorstellen und Informationen zusammentragen.
* diese Informationen hier zusammenfassen und
* eine digitale Karte unter `info.lausitzer-surfer.de/karte` erstellen, welche Informationen leicht zugänglich macht.

Bisher getan:  
- die [Karte](https://info.lausitzer-surfer.de/karte/) ist online. Die Geodaten dazu liegen in mehreren `geojson`-Dateien im [Framagit](https://framagit.org/lausitzer-surfer-eV/info.lausitzer-surfer.de/-/tree/main/content/karte?ref_type=heads)
- die wichtigsten [Spots](https://info.lausitzer-surfer.de/spots/) sind erfasst. Hier fehlen teilweise noch Beschreibungen und Infos zu Regeln.

Noch zu tun:
- Regeln ergänzen und weitere Gewässer unter Spots auflisten

### [Luftaufnahmen](#h-luftbildaufnahmen)

- für die [Spots](https://info.lausitzer-surfer.de/spots/) Seite und die Gewässer- und Spotbeschreibungen wäre es toll, Luftbildaufnahmen zu haben
- bitte meldet Euch, falls Ihr eine Drohne habt oder Luftbildaufnahmen machen könnt. Ziel ist es, den jeweiligen Strandabschnitt so zu fotografieren, dass die Bereiche (Launch, Reach) gut erkennbar sind.

### [Logo](#h-logo)

Wir suchen jemanden, der sich mit der Erstellung des Logos beschäftigt. Ziel ist es,
alle Surf-Sportarten gleichermaßen abzubilden. Unser Platzhalter bis dahin:

{{< rawhtml >}}

<div id="hero">
  <img src="https://lausitzer-surfer.de/images/logo.png" width="200px"></img>
</div>
  
{{< /rawhtml >}}

Update 03.08.2024: Die ersten Vorschläge sind [da!](https://framagit.org/lausitzer-surfer-eV/website/-/issues/1)

### [Senftenberg Geschwindigkeitsbegrenzung](#h-SFB-Geschwindigkeit)

Möglicherweise gibt es einen Antrag am Senftenberger See die Geschwindigkeitsbegrenzung für Motorbote abzuschaffen.
Das könnte bei dem ohnehin schon knappen Platzangebot in Senftenberg zu weiteren Problemen mit Surfern und Schwimmern
führen. Hier wollen wir Kontakt zu Entscheidungsträgern aufnehmen.