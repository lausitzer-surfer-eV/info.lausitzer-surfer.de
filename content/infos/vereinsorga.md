+++
title = "Vereinsorga"
description = "Hier findet Ihr Informationen zur Selbstorganisation von Mitgliedern, Vorstand und Verein."
weight = 10
draft = false
toc = true
bref = "Selbstorganisation von Mitgliedern, Vorstand und Verein."
+++

### [Informationen ändern und hinzufügen](#h-aendern)

Mitglieder und auch Nicht-Mitglieder können Informationen [infos.lausitzer-surfer.de](https://infos.lausitzer-surfer.de/)
und [lausitzer-surfer.de](https://lausitzer-surfer.de/) gemeinsam ändern.

Alle Informationen für die Webseiten werden unter [framagit.org/lausitzer-surfer-eV](https://framagit.org/lausitzer-surfer-eV/) verwaltet.

- Um die Webseite zu ändern oder Informationen hinzuzufügen, bitte im Framagit [registrieren](https://framagit.org/users/sign_up) und 
Zugang in der Gruppe [Lausitzer Surfer e.V.](https://framagit.org/lausitzer-surfer-eV) erfragen. 
- Nach der Freischaltung können Änderungen an der Webseite gemacht werden. Wir ergänzen hier noch eine Anleitung
- Nach Änderungen wird die Webseite automatisch erstellt und auf dem Server aktualisiert. Das kann mitunter bis zu 5 Minuten dauern.

Die Geodaten für Karte und Spots liegen in mehreren `geojson`-Dateien:  
- Wasserflächen: [surfbar_WGS1984_cleaned.geojson](https://framagit.org/lausitzer-surfer-eV/info.lausitzer-surfer.de/-/blob/main/content/karte/surfbar_WGS1984_cleaned.geojson?ref_type=heads)  
- Spots: [spots.geojson](https://framagit.org/lausitzer-surfer-eV/info.lausitzer-surfer.de/-/blob/main/content/karte/spots.geojson?ref_type=heads)  
- Detailinfos für spots unter [info.lausitzer-surfer.de/content/spots/
geodaten](https://framagit.org/lausitzer-surfer-eV/info.lausitzer-surfer.de/-/tree/main/content/spots/geodaten?ref_type=heads)  

Einzelne Änderungen an `geojson`-Dateien (Rechtschreibfehler, Namensänderungen) können direkt im Gitlab oder mit einem Texteditor vorgenommen werden. Neue features (POIs etc.)
hinzufügen durch einfügen einer neuen Zeile. Für umfangreichere Änderungen (polygone etc.)
bitte z.B. QGIS nutzen.