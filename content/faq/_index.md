+++
draft= false
title = "FAQ"
description = "Gefragt und beantwortet"
+++

## Warum gehen so viele Links nicht?

Wir sind erst gerade gestartet! Wir ergänzen nach und nach die Lücken.

## Auf der Webseite sind Fehler (z.B. inhaltlich oder funktional)

Die Webseite ist ein fortlaufendes Gemeinschafts-Projekt und Eure Mitarbeit ist sehr wichtig.

Ihr habt verschiedene Möglichkeiten, uns bei Fehlern und deren Berichtigung zu helfen.

- Der einfachste Weg ist eine Email an [info@lausitzer-surfer.de](mailto:info@lausitzer-surfer.de).
- Noch besser ist, wenn Ihr direkt in der `Lausitzer Surfer e.V.` Gruppe im gemeinnützigen [Framagit](https://framagit.org/) ein ein "Issue" anlegt, wo Änderungen diskutiert und abgeschlossen werden können. 

  Dazu bitte im Framagit
  den Zugang zu den jeweiligen Projekten oder der Gruppe erfragen:
    - Das Projekt für die Hauptwebseite [lausitzer-surfer.de](https://lausitzer-surfer.de) könnt
      Ihr [hier](https://framagit.org/lausitzer-surfer-eV/website/-/issues) finden
    - Das Projekt für die Info Webseite [info.lausitzer-surfer.de](https://info.lausitzer-surfer.de) könnt Ihr [hier](https://framagit.org/lausitzer-surfer-eV/info.lausitzer-surfer.de/-/issues) finden
    - Das Projekt für die Mitgliederverwaltung und Selbstregistrierung [mitglieder.lausitzer-surfer.de](https://mitglieder.lausitzer-surfer.de) könnt Ihr [hier](https://framagit.org/lausitzer-surfer-eV/mitglieder-management/account-kc-theme/-/issues) finden

Die Webseiten werden im Anschluß direkt aus den Projekten automatisch gebaut und aktualisiert.

(Wer sich mit `git` auskennt ist natürlich herzlich eingeladen, einen direkten Merge Request
zu stellen!)

Um insbesondere Informationen zu den Spots und Karten zu ergänzen werden wir Euch bald mehr Informationen zum Prozess unter [Vereinsorga](https://info.lausitzer-surfer.de/infos/vereinsorga/#h-aendern) bereitstellen.

## Ich würde gern dem Verein beitreten, habe aber noch Fragen

Schreib uns einfach direkt unter [info@lausitzer-surfer.de](mailto:info@lausitzer-surfer.de). Wir freuen uns sehr, von Dir zu hören und können Dir über Email weitere Austauschmöglichkeiten vorschlagen.