+++
date = "2023-11-30T21:00:00+01:00"
title = "Außerordentliche Mitgliederversammlung"
tags = ["surfen","versammlung"]
categories = ["general"]
draft = false
description = "zur Änderung der Satzung für Anerkennung der Gemeinnützigkeit."
weight = 10
+++

Ja, so einen Verein gründen ist nicht leicht. Wir wollten die Gründung unbedingt
im Oktober bei unserem alljährlichen Surftrip auf Rügen vollziehen, da sich die Gründung
sonst noch länger hinaus gezögert hätte.

In der Kürze war die Satzung schon kurz nach der Gründung überholt. Uns wurde klar,
dass wir als Lausitzer Surfer e.V. überregional tätig sein werden, ohne uns an eine
Lokalität oder einen See zu binden. Außerdem wollen wir alle Surfer vertreten und
möglichst allen gleichberechtigt Mitspracherecht geben, unabhängig wo jedes Mitglied
herkommt. Das bedeutete für uns, unsere Netzwerktätigkeit muss digital ausgerüstet sein.

Hier gibt es neue Möglicheiten für Vereine, zum Beispiel Online-Mitgliederversammlungen durchzuführen, oder Abstimmen den Mitgliedern schon vor den Versammlungen zu ermöglichen,
sodass auch jene, welche an der Versammlung nicht teilnehmen, Ihre Stimme abgeben können.
Ausserdem wollen wir die Selbstorganisation der Mitglieder durch ein Online-System unterstützen
und den  Vorstand von Organisatorischem wie Registrierung, Spendenbescheinigungen ausstellen und Lastschriften einrichten entlasten.

Zuletzt wollten wir die Möglichkeit von Online-Notar Beglaubigungen nutzen. Einfach, weil das
so jetzt gemacht wird. Punkt.

Für all das mussten wir die Satzung anpassen. Hier findet Ihr die Änderungen im
Nachverfolgungsmodus und das Protokoll der Mitgliederversammlung:

{{< rawhtml >}}
<ul>
<li><i class="fas fa-file"></i> <a href="https://lausitzer-surfer.de/pdf/2023-11-30_mgv/01_Satzung_vgl.pdf">Satzungsänderungen vom 31.10.2023</a></li>
<li><i class="fas fa-file"></i> <a href="https://lausitzer-surfer.de/pdf/2023-11-30_mgv/Protokoll_Mitgliederversammlung.pdf">Protokoll der Mitgliederversammlung</a></li>
<li>Die aktuelle Satzung liegt auf <a href="https://lausitzer-surfer.de">lausitzer-surfer.de</a></li>
</ul>
{{< /rawhtml >}}

{{< rawhtml >}}
<figure>
    <img src="https://lausitzer-surfer.de/pdf/2023-11-30_mgv//2023-10-30_bbb.webp" 
      alt="Big Blue Button">
<figcaption>Online Mitgliederversammlung in BigBlueButton, mit freundlicher Genehmigung der TUD</figcaption>
</figure>
{{< /rawhtml >}}

--------

**Fotos**

Natürlich wurde auch gesurft! Hier ein paar Fotos vom Oktober, vom Berzdorfer See.

{{< rawhtml >}}
<div class="image-container">
  <div class="photo"><img src="../img/2023-10-14_berzdorfer1.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer2.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer3.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer4.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer5.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer6.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer7.webp"></div>
  <div class="photo"><img src="../img/2023-10-14_berzdorfer8.webp"></div>
</div>
{{< /rawhtml >}}

--------

Viele Grüße

> Euer Vorstand  
> Vorsitzender Alexander Dunkel  
> Stellvertretender Vorsitzender Kristof Ahnert  
> Schatzmeister Dr. Steve Kupke  
> Datenschutzbeauftragter Thomas Forner  
