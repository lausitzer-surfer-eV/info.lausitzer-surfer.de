+++
date = "2024-08-14T4:00:55+01:00"
title = "Aktuelles und Treffen am Senftenberger See"
tags = ["treffen","senftenberg", "aktuelles"]
categories = ["general"]
draft = false
description = "2024: Aktuelles und Treffen am Senftenberger See, Buchwalde."
weight = 10
+++

**Unser Motto für 2024: Es dauert so lange wie es dauert.** Nein, in echt, wer
hätte gedacht, dass einen Verein gründen fast ein Jahr in Anspruch nehmen würde?

Aber es ist so und es passt auch. Wir sind fast durch mit den Formalien. 

Uns fehlt nun noch der letzte Baustein, das Vereinsonto. Aber es ist beantragt
und sollte demnächst eingehen. Dann können wir auch Spenden entgegennehmen.

{{< rawhtml >}}
<figure>
    <img src="../img/2024-04-02_SFB.webp" 
      alt="Surfen am Senftenberger See im April">
<figcaption>Surfen am Senftenberger See im April (Sebastian Siwek, Foto Alexander Dunkel)</figcaption>
</figure>

<figure>
    <img src="../img/2024-08-13_Ronneby.webp" 
      alt="Hendrik in Ronneby">
<figcaption>Hendrik in Ronneby, Schweden</figcaption>
</figure>
{{< /rawhtml >}}

**Vielen Dank an die Surf-Community für die vielen positiven Rückmeldungen im 
Laufe des Jahres!** Wir sind ohne den Anspruch gestartet, eine bestimmte 
Mitgliederzahl zu erreichen - wenn es bei uns Sieben geblieben wäre - ok. 
Aber Ihr habt uns das Vertrauen  geschenkt und so sind wir mittlerweile auf 
**23 Mitglieder angewachsen (18 Surfer und 5 Surferinnen).**.



## Treffen am Senftenberger See

Wie in der [SURF][surf] angekündigt, wollen wir uns am 17. August (Samstag) 
ab Mittag am Senftenberger See treffen. Leider hat sich beim Abdruck des 
Artikels ein Fehler eingeschlichen. **Das Treffen findet in Buchwalde statt**, 
nicht wie in der SURF versehentlich verwechselt an der Surfschule!

{{< rawhtml >}}
<link rel='stylesheet' href='/karte/maplibre-gl.css' />
  <script src='/karte/maplibre-gl.js'></script>
{{< /rawhtml >}}

{{< luftbild  
    see="senftenberger"
    center_lng="14.024090"
    center_lat="51.512022"
    zoom="16.78287685031633"
  >}}


Es wird ein informelles Treffen, ohne große Planung. Ich bringe Grillmaterialien mit,
wer Lust und Appetit hat, bringt am besten so viel mit, um den eigenen Hunger und Durst
zu stillen. Das Treffen wird dann gegen 17:00 offiziell zu Ende sein, aber wer will
kann natürlich Abends noch bleiben.

## Geschwindigkeitsbegrenzung am Senftenberger See

Derzeit wird diskutiert, die zulässige Höchstgeschwindigkeit auf dem Senftenberger See, insbesondere für Motorboote, über 15 km/h anzuheben. Der Segelclub 1978 Senftenberg e.V. hat sich an uns gewandt und um Unterzeichnung einer gemeinsamen Stellungnahme gebeten. Die unterzeichnete Stellungnahme findet Ihr [hier](../pdf/2024-08-14_Stellungnahme%20zulässige%20Höchstgeschwindigkeiten_signed.pdf).

Als Surfclub liegt uns die Sicherheit aller Wassersportler am Herzen. Eine
Erhöhung des Tempolimits würde zu häufigeren und schwereren Konflikten auf dem Senftenberger See führen, da der ohnehin knappe Platz auf dem Wasser durch höhere Geschwindigkeiten, insbesondere von Motorbooten, noch problematischer würde. Aus unserer Sicht wäre es im Interesse aller Wassersportler und Schwimmer die derzeitige Geschwindigkeitsbegrenzung beizubehalten.

## Logo

Paulin hat ein Logo für den Lausitzer Surfer e.V. entworfen! Ganz herzlichen Dank für die Initiative, Paulin. ❤️❤️❤️

![](../logo/logo.svg)

{{< rawhtml >}}
<div class="image-container">
  <div class="photo"><img src="../logo/logo2.jpg"></div>
  <div class="photo"><img src="../logo/logo3.jpg"></div>
  <div class="photo"><img src="../logo/logo4.jpg"></div>
  <div class="photo"><img src="../logo/logo5.jpg"></div>
</div>
{{< /rawhtml >}}

Es ist nicht möglich, alle Sportarten gleichberechtigt in einem Logo darzustellen. 
Deshalb hat Paulin mehrere Varianten entworfen. Falls ihr noch nicht _ganz_ 
zufrieden seid: Ein Logo kann sich entwickeln.

Ich werde versuchen, am Samstag ein paar Aufkleber mitzubringen 😊

--------

**Fotos**

Natürlich wurde auch gesurft! Hier ein paar Fotos aus 2024. Die Saison hat
ihren Höhepunkt mit den Herbstürmen noch vor sich!

{{< rawhtml >}}
<div class="image-container">
  <div class="photo"><img src="../img/2024-04-02_SFB1.webp"></div>
  <div class="photo"><img src="../img/2024-04-02_SFB2.webp"></div>
  <div class="photo"><img src="../img/2024-04-02_SFB3.webp"></div>
  <div class="photo"><img src="../img/2024-04-02_SFB4.webp"></div>
  <div class="photo"><img src="../img/2024-05-16_SFB6.webp"></div>
  <div class="photo"><img src="../img/2024-06-30_SFB5.webp"></div>
  <div class="photo"><img src="../img/2024-06-30_SFB8.webp"></div>
  <div class="photo"><img src="../img/2024-05-02_Partwitzer10.webp"></div>
</div>
{{< /rawhtml >}}

--------

**Ein herzliches Willkommen an unsere neuen Mitglieder, Jonas, Christine, Peter, Stefanie, Robert, Jens, Ingolf, Silvio, Frank, Gert, Grit,  & Sebastian!**

--------

Viele Grüße und vielleicht bis nächstes Wochenende am Senftenberger See,  
Alex

> Euer Vorstand  
> Vorsitzender Alexander Dunkel  
> Stellvertretender Vorsitzender Kristof Ahnert  
> Schatzmeister Dr. Steve Kupke  
> Datenschutzbeauftragter Thomas Forner  


[surf]: https://www.surf-magazin.de/spots-und-reviere/seen/spot-guide-lausitzer-seenland-wassersport-paradies-fuer-windsurfer-winger-und-sup/