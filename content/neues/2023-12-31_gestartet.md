+++
date = "2023-12-31T11:10:55+01:00"
title = "2023: Gestartet"
tags = ["markdown","example"]
categories = ["general"]
draft = false
description = "2023 geht zu Ende, Lausitzer Surfer e.V. startet."
weight = 10
+++

Zuerst ein Eindruck vom Partwitzer See von Sebastian von 2024.

{{< rawhtml >}}

<video style="width:100%;height:auto;margin: 0em 0;" controls poster="/vid/2024-01-03_ 15.14.54.png" crossorigin="anonymous">

  <source src="/vid/2024-01-03_ 15.14.54.mp4" type="video/mp4">
  <p>Your browser does not support HTML5 video.</p>
</video>
<figcaption>Partwitzer See Wing Foil im neuen Jahr, Sebastian Siwek</figcaption>

{{< /rawhtml >}}

Auch wenn es natürlich immer zu langsam geht, sind wir auf dem Weg. Die Registrierung beim Amtsgericht
für den "e.V." ist für den 5. Januar 2024 geplant, nachdem wir am 31. Oktober 2023 kurzfristig eine Online-Mitgliederversammlung
anberaumt hatten, um die Satzung kompatibel zu machen. Wir sind die Ersten, welche von der
Online-Notar-Beglaubigung Gebrauch machen! Digitalisierung hurra 😃

Es gibt aber doch ein paar Neuigkeiten zu den Vereinsseiten.

Die Webseite (https://lausitzer-surfer.de/) ist nun gemeinsam bearbeitbar und transparent einsehbar.

Alle Informationen liegen unter https://framagit.org/lausitzer-surfer-eV/. Mitglieder des Vereins können
sich bei Framagit registrieren und den Zugang anfragen, ich schalte Euch dann frei. Anschließend könnt Ihr Seiten bearbeiten.
Framagit ist eine gemeinnützige Instanz von Gitlab.

Die Webseite wird nach Bearbeitung neu gebaut und online verfügbar gemacht.

Ähnlich aufgebaut ist die Unterseite https://infos.lausitzer-surfer.de/. Auch diese Daten liegen in der Framagit Gruppe
und können von allen bearbeitet werden. Es ist derzeit noch ein Skelett, hier müssen noch alle Informationen ergänzt werden. Aber alles der Reihe nach.

Geplant für nächstes Jahr ist dann die Mitglieder-Selbstverwaltung mittels [Hitobito](https://hitobito.com/de/),
welche unter https://orga.lausitzer-surfer.de/ erreichbar sein wird. Hier ist auch denkbar, Untergruppen für die einzelnen Surfsportarten
einzurichten und einen Service für Surfschulen in der Lausitz anzubieten, um z.B. der kostenlos Kurse zu verwalten (cossebaude.lausitzer-surfer.de).
Die Selbstverwaltung wird insbesondere wichtig, wenn es zum "SURF"-Bericht über die Lausitz kommt und neue Mitglieder sich dann selbst registrieren können.

Was noch etwas weiter in der Ferne steht, aber schon in dem Protokoll der Mitgliederversammlung aufgetaucht ist:
- LimeSurvey (Wahlen), `mitbestimmen.lausitzer-surfer.de`
- Nextcloud - Kalender, Filesharing; `cloud.lausitzer-surfer.de`
- Digitale Karte mit aktuellen Gesetzen/Regeln der Lausitzer Gewässer; map.lausitzer-
surfer.de / `karte.lausitzer-surfer.de`

Außerdem brauchen wir noch ein Logo! Ich habe hier erst einmal nur einen Platzhalter verwendet.

![Logo](https://lausitzer-surfer.de/images/logo.png)

Wir werden hier auch weiter versuchen, möglichst offen über alle Änderungen zu berichten
und auch nicht-Mitgliedern alle Informationen zugänglich zu machen. So könnt Ihr euch
einen Einblick über die Vereintstätigkeiten verschaffen, auch bevor ihr eintretet.

--------

**Fotos**

Natürlich wurde auch gesurft! Hier ein paar Fotos gegen Ende des Jahres. 
Am 31. Oktober habe ich Hendrik am Partwitzer getroffen, wo er der Einzige auf dem See war. 
bei 11° Lufttemperatur! Wir waren gute 2 Stunden auf dem Wasser, ich mit Wing und Foil und Hendrik mit SUP. Hendrik war sofort beim Lausitzer Surfer e.V. dabei und vertritt von nun an unsere SUP-Fraktion.

{{< rawhtml >}}
<div class="image-container">
  <div class="photo"><img src="../img/2023-12-31_partwitzer1.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer2.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer3.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer4.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer5.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer6.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer7.webp"></div>
  <div class="photo"><img src="../img/2023-12-31_partwitzer8.webp"></div>
</div>
{{< /rawhtml >}}

--------

**Ein herzliches Willkommen an unsere neuen Mitglieder, Heiko, Hendrik, Paulin & Antonio!**

--------

Viele Grüße und einen guten Start uns allen ins Jahr 2024,  
Alex

> Euer Vorstand  
> Vorsitzender Alexander Dunkel  
> Stellvertretender Vorsitzender Kristof Ahnert  
> Schatzmeister Dr. Steve Kupke  
> Datenschutzbeauftragter Thomas Forner  
