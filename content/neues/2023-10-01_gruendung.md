+++
date = "2023-10-01T19:00:00+01:00"
title = "Gründung vom Lausitzer Surfer e.V."
tags = ["gründung","versammlung"]
categories = ["general"]
draft = false
description = "Dranske, 01. Oktober 2023: 7 Surfer & ein neuer e.V. für die Lausitz"
weight = 10
+++

**Am 01. Oktober 2023, um 18:00 Uhr in Dranske, Rügen, wurde der Lausitzer Surfer e.V. gegründet.** 🌊🏄🌊

Vielen Dank an Alle. Natürlich wurde auch gesurft..! Hier findet Ihr die Zusammenfassung von der Gründungsveranstaltung .. und ein paar Eindrücke.

--------

## Zusammenfassung

Die Lausitz ist ein Surfrevier mit großem Potential für Deutschland, jedoch mit derzeit nur gering vernetzten Surfern. Wir haben gute Surfschulen. Die Abgänger dort nach dem Grundkurs stehen aber erstmal allein da - Wohin? Wann? Bei welchen Windrichtungen? Hier wollen wir mit dem Lausitzer Surfer e.V. ein Netzwerk schaffen, wo sich Windsurfer, Foilsurfer und Wingfoiler und weitere Surfbegeisterte absprechen können. Damit sind genauso Surfer, welche aus den angrenzenden Ländern Polen und Tschechien anreisen, angesprochen.

In den nächsten Jahren wird mit den weiteren eröffneten Seen in der Lausitz viel passieren. Gemeinsam können wir (demokratischen) Einfluss darauf nehmen, wo gute Startbedingungen für Surfer gesichert und geschaffen werden und Strandzugänge und Infrastruktur verbessert werden können. Allgemein sehen wir uns als Ansprechpartner für alle Surferfragen, beispielsweise wo und für wen welche Regeln sinnvoll sind und wie diese Regeln kommuniziert werden können.

Unsere drei prioritäten Ziele:

- Zugänge für wichtige Windsurf-Strände im Lausitzer Seenland sichern, 
   erhalten und entwickeln
- Demokratische Interessenvertretung für politische Fragestellungen
- Vernetzung, Nachwuchsförderung

> Dranske, Rügen, 01. Oktober 2023

**Zum Vorstand gewählt wurden:**

- Vorsitzender Dr. Alexander Dunkel  
- Stellvertretender Vorsitzender Kristof Ahnert  
- Schatzmeister Dr. Steve Kupke  
- Datenschutzbeauftragter Thomas Forner  

**Die sieben Gründungsmitglieder nach Anlage 1 Gründungsprotokoll sind:**

| **LfdNr.** | **Nachname** | **Vorname** |
| ---------- | ------------ | ----------- |
| 1          | Dunkel   | Alexander   |
| 2          | Heinzig  | André       |
| 3          | Reimelt      | Isabel      |
| 4          | Ahnert       | Kristof     |
| 5          | Wagner       | Philipp     |
| 6          | Kupke    | Steve       |
| 7          | Forner       | Thomas      |

--------

{{< rawhtml >}}
<div class="image-container">
  <div class="photo"><img src="../img/2023-10-01_meeting.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_gmitglieder.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_beach.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_alex.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_drkupke.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_phil.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_drheinzig.webp"></div>
  <div class="photo"><img src="../img/2023-10-01_kristof.webp"></div>
</div>
{{< /rawhtml >}}