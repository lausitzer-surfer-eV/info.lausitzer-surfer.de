+++
date = "2024-01-26T07:05:12+01:00"
draft = false
weight = 140
description = "Regeln und Surfspots am Bergheider See"
title = "Bergheider See"
bref= "Regeln und Surfspots am Bergheider See in der Lausitz"
toc = true
[map]
  zoom = 11.517183481231596
  center_lng = 13.792328968227196
  center_lat = 51.573568734877085
+++

_Hier wird noch gearbeitet._

# [Spot: Nordstrand](#nordstrand) {#nordstrand}
`Windsurfen` `SUP` `Kite-Surfen` `Foil`

|   <!-- -->      |             <!-- -->          |
| ----------------- | -------------------------------------- |
| **Geeignet für**: | Windsurfer, SUP, Fortgeschrittene          |
| **Revier**:       | Sehr breiter Strand, schwieriger Zugang vom Parkplatz, Flachwasser |
| **Windrichtung**: | West und Ost; bei Süd machbar|

{{< luftbild  
    see="bergheider"
    center_lng="  13.791123"
    center_lat="51.582075"
    zoom="13.590271231545663"
  >}}