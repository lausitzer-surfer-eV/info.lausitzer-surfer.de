+++
date = "2024-01-26T07:05:12+01:00"
draft = false
weight = 140
description = "Spotguide"
title = "Senftenberger See"
bref= "Regeln und Surfspots am Senftenberger See in der Lausitz"
toc = true
[map]
  zoom = 11.511438425158167
  center_lng = 14.01424089096787
  center_lat = 51.49594451391204
+++

Der Senftenberger See ist einer der wichtigsten Seen für Windsurfer und Surfer in der Lausitz, wobei durch die neueren Seen die Bedeutung von "SFB" langsam zurückgeht. Der See ist gut ausgebaut. Es gibt eine Surfschule; [Buchwalde](#buchwalde) als Hauptsurfspot bietet gute Parkmöglichkeiten.

**Fahrverbot**: Vom 01.11. bis 31.03. ist das befahren (SUP, Foil, Surf etc) des Senftenberger Sees verboten. Grund ist, dass die Tonnage für die Sperrzone
im Winter reingeholt wird und so für die WSP nicht kontrollierbar ist, ob
Surfer in die Sperrzone fahren. Wir arbeiten dran, eine einvernehmliche Lösung
zu finden, sodass auch im Winter surfen in SFB möglich wird.

------

# [Spot: Buchwalde](#buchwalde) {#buchwalde}

`Windsurfen` `Foil` `SUP`

|   <!-- -->      |             <!-- -->          |
| ----------------- | -------------------------------------- |
| **Geeignet für**: | Windsurfer, Foiler, SUP, Einsteiger    |
| **Revier**:       | Flachwasser, Stehbereich, relativ kurzer Reach durch Bojen   |
| **Windrichtung**: | Typischer Westwindspot; Süd und Ost fahrbar  |

{{< luftbild  
    see="senftenberger"
    center_lng="14.02455680835321"
    center_lat="51.51065517165928"
    zoom="14.78287685031633"
  >}}

<br><br>

# Quellen

{{< rawhtml >}}

<ul class="quellen">
<li><i class="fas fa-file"></i> <a href="https://www.yumpu.com/de/document/read/6475051/senftenberger-see-schiffbares-gewasser-zweckverband-">Hier</a> findet Ihr weiterführende Informationen und eine Karte vom <code>Zweckverband Lausitzer Seenland Brandenburg</code>.</li>
<li><i class="fas fa-file"></i> <a href="https://partwitzersee.com/wp-content/uploads/2020/04/ZV-LausS_FB-Schiff_gesamt.pdf">Liste der Regeln an Senftenberger See, Geierswalder See und Partwitzer See, Herausgeber Tourismusverband Lausitzer Seenland e.V</a></li>
</ul>

{{< /rawhtml >}}

# Fotos

-------

{{< rawhtml >}}

<div class="image-container">
  <div class="photo"><img src="../img/sfb_01.webp"></div>
  <div class="photo"><img src="../img/sfb_02.webp"></div>
  <div class="photo"><img src="../img/sfb_03.webp"></div>
  <div class="photo"><img src="../img/sfb_04.webp"></div>
</div>

{{< /rawhtml >}}