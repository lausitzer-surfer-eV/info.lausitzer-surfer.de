+++
date = "2024-01-26T07:05:12+01:00"
draft = false
weight = 140
description = "Regeln und Surfspots am Geierswalder See"
title = "Geierswalder See"
bref= "Regeln und Surfspots am Geierswalder See in der Lausitz"
toc = true
[map]
  zoom = 11.836078076486773
  center_lng = 14.113861616561081
  center_lat = 51.50852185199117
+++

_Hier wird noch gearbeitet._

# [Spot: Koschendamm](#koschendamm) {#koschendamm}
`Windsurfen` `SUP` `Kite-Surfen` `Foil`

|   <!-- -->      |             <!-- -->          |
| ----------------- | -------------------------------------- |
| **Geeignet für**: | Kitesurfer, Windsurfer, SUP, Einsteiger            |
| **Revier**:       | Kiterevier im Norden, Kabbelwasser, kurzer Stehbereich |
| **Windrichtung**: | Süd und West |

{{< luftbild  
    see="geierswalder"
    center_lng="14.128378"
    center_lat="51.500341"
    zoom="14.130271231545663"
  >}}

# Quellen

{{< rawhtml >}}
<ul class="quellen">
<li><i class="fas fa-link" ></i> Auf den Seiten <a href="https://www.kitesurf-lausitz.de/spots-regeln/geierswalder-see">des Lausitzer Kitesurf e.V.</a> findet Ihr alles zu den Kitesurf-Regeln am Geierswalder See.</li>
</ul>
{{< /rawhtml >}}

