+++
date = "2024-01-26T07:05:12+01:00"
draft = false
weight = 140
description = "Spotguide"
title = "Partwitzer See"
bref= "Regeln und Surfspots am Partwitzer See in der Lausitz"
toc = true
[map]
  zoom = 11.161441138395262
  center_lng = 14.149197258350625
  center_lat = 51.52636108894478
+++

**Achtung:** Auf dem Partwitzer See ist das Foilen derzeit aus Naturschutzgründen nicht gestattet!

# [Spot: Ostbucht](#ostbucht) {#ostbucht}
`Windsurfen` `SUP`

|   <!-- -->      |             <!-- -->          |
| ----------------- | -------------------------------------- |
| **Geeignet für**: | Windsurfer, SUP, Einsteiger            |
| **Revier**:       | Flachwasser, Kabbelwasser, kurzer Stehbereich |
| **Windrichtung**: | Vor allem Süd und West, teilweise Ost machbar  |

Geparkt wird am Nordstrand gegenüber der Straße. Man muss das Material
durch eine Schranke tragen. Dafür wird man mit türkisblauem Wasser belohnt!
Der Spot ist am besten bei Südwind oder leicht Westwind. Hier kann sich durch
die konusförmige Einengung des Strands in der Mitte der Bucht eine überlagerte Welle bilden,
welche herausfordernd sein kann.

Der Partwitzer See ist insbesondere im Herbst und Frühjahr empfehlenswert, da
der tieferliegende Strand bei Pausen winddeckung bietet und keine Badegäste
vor Ort sind.

{{< luftbild  
    see="partwitzer"
    center_lng="14.170548"
    center_lat="51.524580"
    zoom="14.78287685031633"
  >}}

<br>

# Quellen

{{< rawhtml >}}
<ul class="quellen">
<li><i class="fas fa-file" ></i> <a href="https://partwitzersee.com/wp-content/uploads/2020/04/ZV-LausS_FB-Schiff_gesamt.pdf">Karte Partwitzer See, Herausgeber Tourismusverband Lausitzer Seenland e.V</a></li>
<li><i class="fas fa-file" ></i> <a href="https://partwitzersee.com/wp-content/uploads/2018/07/Masterplan-Koschendamm_Endbericht.pdf">Masterplan Koschendamm (2018)</a></li>
<li><i class="fas fa-file" ></i> <a href="https://www.lmbv.de/wp-content/uploads/2021/04/Partwitzer_See_Tgb_Skado_2010.pdf">Seesteckbrief Partwitzer See, LMBV</a></li>
<li><i class="fas fa-map" ></i> <a href="https://lmbv.maps.arcgis.com/apps/webappviewer/index.html?id=64068d71103d40a9a0a07f6b0682db1c">Karte Sperrbereiche (ArcGis Online), LMBV</a></li>
</ul>
{{< /rawhtml >}}

# Fotos

-------

{{< rawhtml >}}

<div class="image-container">
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer1.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer2.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer3.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer4.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer5.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer6.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer7.webp"></div>
  <div class="photo"><img src="../../neues/img/2023-12-31_partwitzer8.webp"></div>
</div>

{{< /rawhtml >}}