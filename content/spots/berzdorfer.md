+++
date = "2024-01-26T07:05:12+01:00"
draft = false
weight = 140
description = "Regeln und Surfspots am Berzdorfer See"
title = "Berzdorfer See"
bref= "Regeln und Surfspots am Berzdorfer See in der Lausitz"
toc = true
[map]
  zoom = 11.20091291903228
  center_lng = 14.965006663366808
  center_lat = 51.089019238089
+++

**Der Berzdorfer See** ist das Highlight im südöstlichen Zipfel der Lausitz. Neben dem
Senftenberger See ist er der am besten ausgebaute Surfspot. Ihr
könnt direkt an der Seekante in Deutsch-Ossig parken, im Winterhalbjahr sogar kostenlos.
Hier trifft man oft auf viele Surfkumpels aus Tschechien. Bei Wind ist der Spot immer gut
besucht. Besonders bei Südwind ist der Berzdorfer See zu empfehlen (es sei denn, Ihr wollt den Bergheider See auf der anderen Seite der Lausitz erkunden). Bei Westwind ist es böig,
zudem kann der Wind während Sturmtiefs durchziehen aufrund der Lage und Form des Sees plötzlich weg sein.

**Achtung:** Auf dem Berzdorfer See ist das Foilen derzeit aus Naturschutzgründen nicht gestattet! Kiten ist aufgrund der Gefahren in Ufernähe ebenso nicht gestattet.

**Neues:** Derzeit erfolgt die Übergabe des Sees von der LMBV an den Staat Sachsen. Dadurch ändern sich einige Regeln und Gesetze. Die Nutzung regelt die [Allgemeinverfügung der Landesdirektion 
Sachsen](#quellen). Der Berzdorfer See wurde zu einem schiffbares Gewässer. In der Karte unten sind zudem die derzeit ausgewiesenen und diskutierten Naturschutzgebiete eingezeichnet, welche von Surfern unbedingt beachtet werden sollten.

# [Spot: Deutsch-Ossig](#deutsch-ossig) {#deutsch-ossig}
`Windsurfen` `SUP`

{{< rawhtml >}}

<img src="../img/berzdorfer_01.webp">
<figcaption>Deutsch-Ossig Einstieg am Berzdorfer See</figcaption>

{{< /rawhtml >}}



|   <!-- -->      |             <!-- -->          |
| ----------------- | -------------------------------------- |
| **Geeignet für**: | Windsurfer, SUP, Fortgeschrittene          |
| **Revier**:       | Schmaler Einstieg, kurzer Stehbereich, teilw. steinig |
| **Windrichtung**: | Süd; bei leicht West auch möglich, aber dann böiger |

{{< luftbild  
    see="berzdorfer"
    center_lng="14.976135"
    center_lat="51.102761"
    zoom="15.590271231545663"
  >}}

# [Quellen](#quellen) {#quellen}

{{< rawhtml >}}
<ul class="quellen">
<li><i class="fas fa-file" ></i> <a href="https://www.oba.sachsen.de/download/Gewaesserrahmenvereinbarung.pdf">Gewässerrahmenvereinbarung zur Übertragung der Tagebaurestseen im Freistaat Sachsen</a></li>
<li><i class="fas fa-file" ></i> <a href="https://www.lds.sachsen.de/bekanntmachung/?ID=19464&art_param=636">Allgemeinverfügung der Landesdirektion 
Sachsen: Berzdorfer See</a></li>
</ul>
{{< /rawhtml >}}
