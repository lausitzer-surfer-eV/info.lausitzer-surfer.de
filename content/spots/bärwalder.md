+++
date = "2024-01-26T07:05:12+01:00"
draft = false
weight = 140
description = "Regeln und Surfspots am Bärwalder See"
title = "Bärwalder See"
bref= "Regeln und Surfspots am Bärwalder See in der Lausitz"
toc = true
[map]
  zoom = 11.633237360501461
  center_lng = 14.554621311746246
  center_lat = 51.376908307017175
+++

_Hier wird noch gearbeitet._

# [Spot: Klitten](#klitten) {#klitten}
`Windsurfen` `SUP` `Foil`

|   <!-- -->      |             <!-- -->          |
| ----------------- | -------------------------------------- |
| **Geeignet für**: | Windsurfer, SUP, Fortgeschrittene          |
| **Revier**:       | Schmaler Einstieg, kurzer Stehbereich, teilw. steinig |
| **Windrichtung**: | West, Nordwest|

{{< luftbild  
    see="bärwalder"
    center_lng="14.562271"
    center_lat="51.361559"
    zoom="14.590271231545663"
  >}}